﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Collections.Generic;

public class XMLParsing : MonoBehaviour 
{
	private string path;	//path for xml
	private string fileInfo;
	private XmlDocument xmlDoc;	//stores xml document
	private WWW www;
	private TextAsset textXml;
	private List<Champion> Champions;	//list to store each champion in
	private string fileName;		//filename of xml file
	Champion tempChampion = new Champion();	//temp champion for champions
	
	//GUIStyles
	private GUIStyle currentStyle = null;
	public GUIStyle vsStyle;
	public GUIStyle strongAgainstStyle;
	public GUIStyle weakAgainstStyle;
	public GUIStyle counterStyle;
	public GUIStyle itemStyle;
	public GUIStyle	stats;
	public GUIStyle stats2;
	public GUIStyle tips;
	
	//GUISkins
	private GUISkin vsSkin;
	
	//textures
	private Texture champPortraitSmallL, champPortraitSmallR,	//two champions on genie
	champPortraitLargeL, champPortraitLargeR;
	private Texture AatroxF, AhriF;
	private Texture title;										//lol genie title
	private Texture arrow;										//arrow button for champ selection
	private Texture arrowR;										//arrow button for champ selection
	private Texture you;
	private Texture enemy;
	private Texture selectG;
	private Texture selectR;

	//booleans
	private bool selecting; //bool for a champ being selected
	private bool selectingLeft, selectingRight;	//bool for which champion is being selected
	private bool defaultC;

	//strings
	string tempString;
	string champL;
	string champR;
	string leftTip;	//stores left tip
	string rightTip;	//stores right tip

	//Champions for each section
	Champion champLeft;
	Champion champRight;

	//floats
	public float level;		//level of champion for stats
	public float level2;

	//ints
	private int champSelectPg;									//page number for the genie champs #1 - #4 
	private int tipNoL;
	private int tipNoR;

	//GUIContent for portrait on buttons
	GUIContent content = new GUIContent();
	public Texture face;
	private Texture[] counterL = new Texture[5];		//counter portrait
	private Texture[] counterR = new Texture[5];		
	private Texture[] countersL = new Texture[5];		//counters portrait
	private Texture[] countersR = new Texture[5];
	private Texture[] icounterL = new Texture[5];		//item counters portrait
	private Texture[] icounterR = new Texture[5];
	private Texture[] topitemL = new Texture[5];		//top items portrait
	private Texture[] topitemR = new Texture[5];

	// Structure for maintaining the Champion information
	struct Champion
	{
		public int Id;
		public string name;
		public string desc;
		public float health;
		public float healthperlvl;
		public float healthregen;
		public float healthregenperlvl;
		public float mana;
		public float manaperlvl;
		public float manaregen;
		public float manaregenperlvl;
		public float range;
		public float attackdamage;
		public float attackdamageperlvl;
		public float attackspeed;
		public float attackspeedperlvl;
		public float armour; 
		public float armourperlvl;
		public float magicres;
		public float magicresperlvl;
		public float movspeed;
		public string portrait;
		public string portraitL;
		public string face;
		public string counter1;
		public string counter2;
		public string counter3;
		public string counter4;
		public string counter5;
		public string counters1;
		public string counters2;
		public string counters3;
		public string counters4;
		public string counters5;
		public string icounter1;
		public string icounter2;
		public string icounter3;
		public string icounter4;
		public string icounter5;
		public string topitem1;
		public string topitem2;
		public string topitem3;
		public string topitem4;
		public string topitem5;
		public string tip1;
		public string tip2;
		public string tip3;
		public string tip4;
		public string tip5;
	};
	
	void OnGUI()
	{
		//Styles
		vsStyle = new GUIStyle(GUI.skin.button);
		vsStyle.normal.textColor = Color.white;
		vsStyle.active.textColor  = Color.cyan;
		
		//loading textures for genie
		AatroxF = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/default");
		AhriF = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/Ahri");
		title = (Texture)Resources.Load ("Textures/Titles/title");
		arrow = (Texture)Resources.Load ("Textures/Titles/arrow");
		arrowR = (Texture)Resources.Load ("Textures/Titles/arrow2");
		you = (Texture)Resources.Load ("Textures/Titles/you");
		enemy = (Texture)Resources.Load ("Textures/Titles/enemy");
		selectG = (Texture)Resources.Load ("Textures/Titles/selectG");
		selectR = (Texture)Resources.Load ("Textures/Titles/selectR");

		//two champions on genie
		//you
		GUI.DrawTexture(new Rect(Screen.width * (0.05f),Screen.height * (0.05f),Screen.width * (0.15f), Screen.height * (0.1f)), you, ScaleMode.ScaleToFit, true);
		GUI.DrawTexture(new Rect(Screen.width * (0.05f),Screen.height * (0.20f),Screen.width * (0.15f), Screen.height * (0.35f)), champPortraitSmallL, ScaleMode.StretchToFill, true);
		//enemy
		GUI.DrawTexture(new Rect(Screen.width * (0.80f),Screen.height * (0.05f),Screen.width * (0.15f), Screen.height * (0.1f)), enemy, ScaleMode.ScaleToFit, true);
		GUI.DrawTexture(new Rect(Screen.width * (0.80f),Screen.height * (0.20f),Screen.width * (0.15f), Screen.height * (0.35f)), champPortraitSmallR, ScaleMode.StretchToFill, true);

		if (defaultC) 
		{
			GUI.DrawTexture(new Rect(Screen.width * (0.80f),Screen.height * (0.20f),Screen.width * (0.15f), Screen.height * (0.35f)), AatroxF, ScaleMode.StretchToFill, true);
		}

		//draw title lol genie
		//GUI.DrawTexture(new Rect(Screen.width * (0.25f),Screen.height * (0.05f),Screen.width * (0.5f), Screen.height * (0.10f)), title, ScaleMode.StretchToFill, true);
		if (GUI.Button (new Rect(Screen.width * (0.25f),Screen.height * (0.05f),Screen.width * (0.5f), Screen.height * (0.10f)),title))
		{
			Application.LoadLevel("HomePage");		//go back to home screen when title is pressed
		}

		//button for 2 champions in genie
		Color temp = GUI.backgroundColor;
		//GUI.color = new Color(1,1,1,0.4f);
		GUI.backgroundColor = new Color (1, 1, 1, 0.1f);

		//left champ
		if (GUI.Button (new Rect (Screen.width * (0.05f), Screen.height * (0.20f), Screen.width * (0.15f), Screen.height * (0.35f)), champLeft.name, vsStyle)) 
		{
			selecting = true;
			selectingLeft = true;
			selectingRight = false;
			//defaultC = false;
		}
		//right champ
		if (GUI.Button (new Rect (Screen.width * (0.80f), Screen.height * (0.20f), Screen.width * (0.15f), Screen.height * (0.35f)), champRight.name, vsStyle)) 
		{
			selecting = true;
			selectingLeft = false;
			selectingRight = true;
			defaultC = false;
		}
		//arrow button
		if (selecting) {
						
			if (GUI.Button (new Rect (Screen.width * (0.425f), Screen.height * (0.375f), Screen.width * (0.15f), Screen.height * (0.15f)), arrow))
					{
						champSelectPg++;		
						if (champSelectPg > 4)
						{
							champSelectPg = 1;
						}
					}
			if (GUI.Button (new Rect (Screen.width * (0.425f), Screen.height * (0.20f), Screen.width * (0.15f), Screen.height * (0.15f)), arrowR))
			{
				champSelectPg--;		
				if (champSelectPg < 1)
				{
					champSelectPg = 4;
				}
			}
				}
		GUI.backgroundColor = temp;

		//buttons for selecting new champions
		if (selecting) {
			float offset = 0.0f;
			float offsetTwo = 0.0f;
			float offsetThree = 0.0f;
			float offsetFour = 0.0f;
			//float offset, offsetTwo, offsetThree, offsetFour = 0.0f;
			bool newLine = false;
			for (int i = 0; i < Champions.Count; i++)
			{
				face = (Texture)Resources.Load(Champions[i].face);
				switch (champSelectPg)
				{
				case 1:
					if (i < 9)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offset), Screen.height * (0.55f), Screen.width * (0.1f), Screen.height * (0.1f)), face)) {
							if (selectingLeft) {
							champPortraitSmallL = (Texture)Resources.Load (Champions[i].portrait);
							champLeft = Champions[i];
						} else {
							champPortraitSmallR = (Texture)Resources.Load (Champions[i].portrait);
							champRight = Champions[i];
						}
							selecting = false;
						}
						offset += 0.1f;
					}

					if (9 <= i && i < 18)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offsetTwo), Screen.height * (0.65f), Screen.width * (0.1f), Screen.height * (0.1f)), face)) {
							if (selectingLeft) {
								champPortraitSmallL = (Texture)Resources.Load (Champions[i].portrait);
								champLeft = Champions[i];
							} else {
								champPortraitSmallR = (Texture)Resources.Load (Champions[i].portrait);
								champRight = Champions[i];
							}
							selecting = false;
						}
						offsetTwo += 0.1f;
					}

					if (18 <= i && i < 27)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offsetThree), Screen.height * (0.75f), Screen.width * (0.1f), Screen.height * (0.1f)), face)) {
							if (selectingLeft) {
								champPortraitSmallL = (Texture)Resources.Load (Champions[i].portrait);
								champLeft = Champions[i];
							} else {
								champPortraitSmallR = (Texture)Resources.Load (Champions[i].portrait);
								champRight = Champions[i];
							}
							selecting = false;
						}
						offsetThree += 0.1f;
					}

					if (27 <= i && i < 36)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offsetFour), Screen.height * (0.85f), Screen.width * (0.1f), Screen.height * (0.1f)), face)) {
							if (selectingLeft) {
								champPortraitSmallL = (Texture)Resources.Load (Champions[i].portrait);
								champLeft = Champions[i];
							} else {
								champPortraitSmallR = (Texture)Resources.Load (Champions[i].portrait);
								champRight = Champions[i];
							}
							selecting = false;
						}
						offsetFour += 0.1f;
					}
					break;
				case 2:
					if (36 <= i && i < 45)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offset), Screen.height * (0.55f), Screen.width * (0.1f), Screen.height * (0.1f)), face)) {
							if (selectingLeft) {
								champPortraitSmallL = (Texture)Resources.Load (Champions[i].portrait);
								champLeft = Champions[i];
							} else {
								champPortraitSmallR = (Texture)Resources.Load (Champions[i].portrait);
								champRight = Champions[i];
							}
							selecting = false;
						}
						offset += 0.1f;
					}
					
					if (/*54*/45 <= i && i < 54/*63*/)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offsetTwo), Screen.height * (0.65f), Screen.width * (0.1f), Screen.height * (0.1f)), face)) {
							if (selectingLeft) {
								champPortraitSmallL = (Texture)Resources.Load (Champions[i].portrait);
								champLeft = Champions[i];
							} else {
								champPortraitSmallR = (Texture)Resources.Load (Champions[i].portrait);
								champRight = Champions[i];
							}
							selecting = false;
						}
						offsetTwo += 0.1f;
					}
					
					if (/*72*/54 <= i && i < /*81*/63)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offsetThree), Screen.height * (0.75f), Screen.width * (0.1f), Screen.height * (0.1f)), face)) {
							if (selectingLeft) {
								champPortraitSmallL = (Texture)Resources.Load (Champions[i].portrait);
								champLeft = Champions[i];
							} else {
								champPortraitSmallR = (Texture)Resources.Load (Champions[i].portrait);
								champRight = Champions[i];
							}
							selecting = false;
						}
						offsetThree += 0.1f;
					}
					
					if (/*90*/63 <= i && i < /*99*/72)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offsetFour), Screen.height * (0.85f), Screen.width * (0.1f), Screen.height * (0.1f)), face)) {
							if (selectingLeft) {
								champPortraitSmallL = (Texture)Resources.Load (Champions[i].portrait);
								champLeft = Champions[i];
							} else {
								champPortraitSmallR = (Texture)Resources.Load (Champions[i].portrait);
								champRight = Champions[i];
							}
							selecting = false;
						}
						offsetFour += 0.1f;
					}
					break;
				case 3:
					if (/*99*/72 <= i && i < 81/*108*/)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offset), Screen.height * (0.55f), Screen.width * (0.1f), Screen.height * (0.1f)), face)) {
							if (selectingLeft) {
								champPortraitSmallL = (Texture)Resources.Load (Champions[i].portrait);
								champLeft = Champions[i];
							} else {
								champPortraitSmallR = (Texture)Resources.Load (Champions[i].portrait);
								champRight = Champions[i];
							}
							selecting = false;
						}
						offset += 0.1f;
					}
					
					if (/*117*/81 <= i && i < /*126*/90)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offsetTwo), Screen.height * (0.65f), Screen.width * (0.1f), Screen.height * (0.1f)), face)) {
							if (selectingLeft) {
								champPortraitSmallL = (Texture)Resources.Load (Champions[i].portrait);
								champLeft = Champions[i];
							} else {
								champPortraitSmallR = (Texture)Resources.Load (Champions[i].portrait);
								champRight = Champions[i];
							}
							selecting = false;
						}
						offsetTwo += 0.1f;
					}
					
					if (/*135*/90 <= i && i < /*144*/99)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offsetThree), Screen.height * (0.75f), Screen.width * (0.1f), Screen.height * (0.1f)), face)) {
							if (selectingLeft) {
								champPortraitSmallL = (Texture)Resources.Load (Champions[i].portrait);
								champLeft = Champions[i];
							} else {
								champPortraitSmallR = (Texture)Resources.Load (Champions[i].portrait);
								champRight = Champions[i];
							}
							selecting = false;
						}
						offsetThree += 0.1f;
					}
					
					if (/*153*/99 <= i && i < /*162*/108)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offsetFour), Screen.height * (0.85f), Screen.width * (0.1f), Screen.height * (0.1f)), face)) {
							if (selectingLeft) {
								champPortraitSmallL = (Texture)Resources.Load (Champions[i].portrait);
								champLeft = Champions[i];
							} else {
								champPortraitSmallR = (Texture)Resources.Load (Champions[i].portrait);
								champRight = Champions[i];
							}
							selecting = false;
						}
						offsetFour += 0.1f;
					}
					break;

				case 4:
					if (/*99*/108 <= i && i < 117/*108*/)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offset), Screen.height * (0.55f), Screen.width * (0.1f), Screen.height * (0.1f)), face)) {
							if (selectingLeft) {
								champPortraitSmallL = (Texture)Resources.Load (Champions[i].portrait);
								champLeft = Champions[i];
							} else {
								champPortraitSmallR = (Texture)Resources.Load (Champions[i].portrait);
								champRight = Champions[i];
							}
							selecting = false;
						}
						offset += 0.1f;
					}
					
					if (/*117*/117 <= i && i < /*126*/126)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offsetTwo), Screen.height * (0.65f), Screen.width * (0.1f), Screen.height * (0.1f)), face)) {
							if (selectingLeft) {
								champPortraitSmallL = (Texture)Resources.Load (Champions[i].portrait);
								champLeft = Champions[i];
							} else {
								champPortraitSmallR = (Texture)Resources.Load (Champions[i].portrait);
								champRight = Champions[i];
							}
							selecting = false;
						}
						offsetTwo += 0.1f;
					}

					break;
				}

			}

		}
		else 
		{
			//Labels for builds
			GUI.Box(new Rect (Screen.width * (0.45f), Screen.height * (0.58f), Screen.width * (0.1f), Screen.height * 0.1f), "", weakAgainstStyle);
			GUI.Box(new Rect (Screen.width * (0.45f), Screen.height * (0.68f), Screen.width * (0.1f), Screen.height * 0.1f), "", strongAgainstStyle);
			GUI.Box(new Rect (Screen.width * (0.45f), Screen.height * (0.78f), Screen.width * (0.1f), Screen.height * 0.1f), "", counterStyle);
			GUI.Box(new Rect (Screen.width * (0.45f), Screen.height * (0.88f), Screen.width * (0.1f), Screen.height * 0.1f), "", itemStyle);

			//level label
			//GUI.Label(new Rect (Screen.width * (0.45f), Screen.height * (0.15f), Screen.width * (0.1f), Screen.height * 0.033f), level.ToString());
			GUI.Box(new Rect (Screen.width * (0.45f), Screen.height * (0.165f), Screen.width * (0.1f), Screen.height * 0.033f), "LEVEL", stats);
			GUI.Box(new Rect (Screen.width * (0.35f), Screen.height * (0.165f), Screen.width * (0.1f), Screen.height * 0.033f), level.ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.55f), Screen.height * (0.165f), Screen.width * (0.1f), Screen.height * 0.033f), level2.ToString(), stats2);

			//champion statistics names
			GUI.Box(new Rect (Screen.width * (0.45f), Screen.height * (0.20f), Screen.width * (0.1f), Screen.height * 0.033f), "HEALTH", stats);
			GUI.Box(new Rect (Screen.width * (0.45f), Screen.height * (0.235f), Screen.width * (0.1f), Screen.height * 0.033f), "HEALTH-REGEN", stats);
			GUI.Box(new Rect (Screen.width * (0.45f), Screen.height * (0.27f), Screen.width * (0.1f), Screen.height * 0.033f), "MANA", stats);
			GUI.Box(new Rect (Screen.width * (0.45f), Screen.height * (0.305f), Screen.width * (0.1f), Screen.height * 0.033f), "MANA-REGEN", stats);
			GUI.Box(new Rect (Screen.width * (0.45f), Screen.height * (0.34f), Screen.width * (0.1f), Screen.height * 0.033f), "RANGE", stats);
			GUI.Box(new Rect (Screen.width * (0.45f), Screen.height * (0.375f), Screen.width * (0.1f), Screen.height * 0.033f), "ATTACK DAMAGE", stats);
			GUI.Box(new Rect (Screen.width * (0.45f), Screen.height * (0.41f), Screen.width * (0.1f), Screen.height * 0.033f), "ATTACK SPEED", stats);
			GUI.Box(new Rect (Screen.width * (0.45f), Screen.height * (0.445f), Screen.width * (0.1f), Screen.height * 0.033f), "ARMOUR", stats);
			GUI.Box(new Rect (Screen.width * (0.45f), Screen.height * (0.48f), Screen.width * (0.1f), Screen.height * 0.033f), "MAGIC RESIST", stats);
			GUI.Box(new Rect (Screen.width * (0.45f), Screen.height * (0.515f), Screen.width * (0.1f), Screen.height * 0.033f), "MOVEMENT SPEED", stats);
			
			//champion descriptions
			GUI.Box(new Rect (Screen.width * (0.05f), Screen.height * (0.55f), Screen.width * (0.15f), Screen.height * 0.033f), "");
			GUI.Box(new Rect (Screen.width * (0.80f), Screen.height * (0.55f), Screen.width * (0.15f), Screen.height * 0.033f), "");
			GUI.Box(new Rect (Screen.width * (0.05f), Screen.height * (0.55f), Screen.width * (0.15f), Screen.height * 0.033f), champLeft.desc);
			GUI.Box(new Rect (Screen.width * (0.80f), Screen.height * (0.55f), Screen.width * (0.15f), Screen.height * 0.033f), champRight.desc);

			//champion tips
			//L
			GUI.Box(new Rect(Screen.width * (0.2f), Screen.height * (0.25f), Screen.width * (0.15f), Screen.height * 0.3f), "", stats2);
			GUI.Box(new Rect (Screen.width * (0.2f), Screen.height * (0.35f), Screen.width * (0.15f), Screen.height * 0.033f), leftTip, tips);
			if (GUI.Button (new Rect(Screen.width * (0.2f), Screen.height * (0.48f), Screen.width * (0.15f), Screen.height * 0.1f), "NEXT", stats))
			{
				tipNoL++;
				if(tipNoL > 5)
				{
					tipNoL = 1;		
				}
			}
			//switch statement to display each tip
			switch(tipNoL)
			{
				case 1:
					leftTip = champLeft.tip1;
					break;
				case 2:
					leftTip = champLeft.tip2;
					break;
				case 3:
					leftTip = champLeft.tip3;
					break;
				case 4:
					leftTip = champLeft.tip4;
					break;
				case 5: 
					leftTip = champLeft.tip5;
					break;
			}
			//R
			GUI.Box(new Rect(Screen.width * (0.65f), Screen.height * (0.25f), Screen.width * (0.15f), Screen.height * 0.3f), "", stats2);
			GUI.Box(new Rect (Screen.width * (0.65f), Screen.height * (0.35f), Screen.width * (0.15f), Screen.height * 0.033f), rightTip, tips);
			if (GUI.Button (new Rect(Screen.width * (0.65f), Screen.height * (0.48f), Screen.width * (0.15f), Screen.height * 0.1f), "NEXT", stats))
			{
				tipNoR++;
				if(tipNoR > 5)
				{
					tipNoR = 1;		
				}
			}
			switch(tipNoR)
			{
			case 1:
				rightTip = champRight.tip1;
				break;
			case 2:
				rightTip = champRight.tip2;
				break;
			case 3:
				rightTip = champRight.tip3;
				break;
			case 4:
				rightTip = champRight.tip4;
				break;
			case 5: 
				rightTip = champRight.tip5;
				break;
			}
		

			//Fill the sections
			Color tempTwo = GUI.color;	//change colour for text
			GUI.color = Color.white;
			//GUI.skin.label.wordWrap = true;
			//champion 1 stats
			GUI.Box(new Rect (Screen.width * (0.35f), Screen.height * (0.20f), Screen.width * (0.1f), Screen.height * 0.033f), (champLeft.health + (champLeft.healthperlvl * level)).ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.35f), Screen.height * (0.235f), Screen.width * (0.1f), Screen.height * 0.033f), (champLeft.healthregen + (champLeft.healthregenperlvl * level)).ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.35f), Screen.height * (0.27f), Screen.width * (0.1f), Screen.height * 0.033f), (champLeft.mana + (champLeft.manaperlvl * level)).ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.35f), Screen.height * (0.305f), Screen.width * (0.1f), Screen.height * 0.033f), (champLeft.manaregen + (champLeft.manaregenperlvl * level)).ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.35f), Screen.height * (0.34f), Screen.width * (0.1f), Screen.height * 0.033f), champLeft.range.ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.35f), Screen.height * (0.375f), Screen.width * (0.1f), Screen.height * 0.033f), (champLeft.attackdamage + (champLeft.attackdamageperlvl * level)).ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.35f), Screen.height * (0.41f), Screen.width * (0.1f), Screen.height * 0.033f), (champLeft.attackspeed + (champLeft.attackspeedperlvl * level)).ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.35f), Screen.height * (0.445f), Screen.width * (0.1f), Screen.height * 0.033f), (champLeft.armour + (champLeft.armourperlvl * level)).ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.35f), Screen.height * (0.48f), Screen.width * (0.1f), Screen.height * 0.033f), (champLeft.magicres + (champLeft.magicresperlvl * level)).ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.35f), Screen.height * (0.515f), Screen.width * (0.1f), Screen.height * 0.033f), champLeft.movspeed.ToString(), stats2); 

			GUI.Box(new Rect (Screen.width * (0.55f), Screen.height * (0.20f), Screen.width * (0.1f), Screen.height * 0.033f), (champRight.health + (champRight.healthperlvl * level2)).ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.55f), Screen.height * (0.235f), Screen.width * (0.1f), Screen.height * 0.033f), (champRight.healthregen + (champRight.healthregenperlvl * level2)).ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.55f), Screen.height * (0.27f), Screen.width * (0.1f), Screen.height * 0.033f), (champRight.mana + (champRight.manaperlvl * level2)).ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.55f), Screen.height * (0.305f), Screen.width * (0.1f), Screen.height * 0.033f), (champRight.manaregen + (champRight.manaregenperlvl * level2)).ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.55f), Screen.height * (0.34f), Screen.width * (0.1f), Screen.height * 0.033f), champRight.range.ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.55f), Screen.height * (0.375f), Screen.width * (0.1f), Screen.height * 0.033f), (champRight.attackdamage + (champRight.attackdamageperlvl * level2)).ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.55f), Screen.height * (0.41f), Screen.width * (0.1f), Screen.height * 0.033f), (champRight.attackspeed + (champRight.attackspeedperlvl * level2)).ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.55f), Screen.height * (0.445f), Screen.width * (0.1f), Screen.height * 0.033f), (champRight.armour + (champRight.armourperlvl * level2)).ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.55f), Screen.height * (0.48f), Screen.width * (0.1f), Screen.height * 0.033f), (champRight.magicres + (champRight.magicresperlvl * level2)).ToString(), stats2);
			GUI.Box(new Rect (Screen.width * (0.55f), Screen.height * (0.515f), Screen.width * (0.1f), Screen.height * 0.033f), champRight.movspeed.ToString(), stats2); 

			GUI.color = tempTwo;	//restore original colour

			//Genie section
			//counter champions
			//L
			counterL[0] = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/" + champLeft.counter1);
			counterL[1] = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/" + champLeft.counter2);
			counterL[2] = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/" + champLeft.counter3);
			counterL[3] = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/" + champLeft.counter4);
			counterL[4] = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/" + champLeft.counter5);
			//R
			counterR[0] = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/" + champRight.counter1);
			counterR[1] = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/" + champRight.counter2);
			counterR[2] = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/" + champRight.counter3);
			counterR[3] = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/" + champRight.counter4);
			counterR[4] = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/" + champRight.counter5);
			//countered champions
			//L
			countersL[0] = (Texture)Resources.Load ("Textures/Champions/ChampionPortraits/" + champLeft.counters1);
			countersL[1] = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/" + champLeft.counters2);
			countersL[2] = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/" + champLeft.counters3);
			countersL[3] = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/" + champLeft.counters4);
			countersL[4] = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/" + champLeft.counters5);
			//R
			countersR[0] = (Texture)Resources.Load ("Textures/Champions/ChampionPortraits/" + champRight.counters1);
			countersR[1] = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/" + champRight.counters2);
			countersR[2] = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/" + champRight.counters3);
			countersR[3] = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/" + champRight.counters4);
			countersR[4] = (Texture)Resources.Load("Textures/Champions/ChampionPortraits/" + champRight.counters5);
			//item counters
			//L
			icounterL[0] = (Texture)Resources.Load ("Textures/Champions/ItemPortraits/" + champLeft.icounter1);
			icounterL[1] = (Texture)Resources.Load("Textures/Champions/ItemPortraits/" + champLeft.icounter2);
			icounterL[2] = (Texture)Resources.Load("Textures/Champions/ItemPortraits/" + champLeft.icounter3);
			icounterL[3] = (Texture)Resources.Load("Textures/Champions/ItemPortraits/" + champLeft.icounter4);
			icounterL[4] = (Texture)Resources.Load("Textures/Champions/ItemPortraits/" + champLeft.icounter5);
			//R
			icounterR[0] = (Texture)Resources.Load ("Textures/Champions/ItemPortraits/" + champRight.icounter1);
			icounterR[1] = (Texture)Resources.Load("Textures/Champions/ItemPortraits/" + champRight.icounter2);
			icounterR[2] = (Texture)Resources.Load("Textures/Champions/ItemPortraits/" + champRight.icounter3);
			icounterR[3] = (Texture)Resources.Load("Textures/Champions/ItemPortraits/" + champRight.icounter4);
			icounterR[4] = (Texture)Resources.Load("Textures/Champions/ItemPortraits/" + champRight.icounter5);
			//item counters
			//L
			topitemL[0] = (Texture)Resources.Load ("Textures/Champions/ItemPortraits/" + champLeft.topitem1);
			topitemL[1] = (Texture)Resources.Load("Textures/Champions/ItemPortraits/" + champLeft.topitem2);
			topitemL[2] = (Texture)Resources.Load("Textures/Champions/ItemPortraits/" + champLeft.topitem3);
			topitemL[3] = (Texture)Resources.Load("Textures/Champions/ItemPortraits/" + champLeft.topitem4);
			topitemL[4] = (Texture)Resources.Load("Textures/Champions/ItemPortraits/" + champLeft.topitem5);
			//R
			topitemR[0] = (Texture)Resources.Load ("Textures/Champions/ItemPortraits/" + champRight.topitem1);
			topitemR[1] = (Texture)Resources.Load("Textures/Champions/ItemPortraits/" + champRight.topitem2);
			topitemR[2] = (Texture)Resources.Load("Textures/Champions/ItemPortraits/" + champRight.topitem3);
			topitemR[3] = (Texture)Resources.Load("Textures/Champions/ItemPortraits/" + champRight.topitem4);
			topitemR[4] = (Texture)Resources.Load("Textures/Champions/ItemPortraits/" + champRight.topitem5);

			float offset = 0.0f;
			for (int i = 0; i < 6; i++)
			{
				//L
				GUI.Box(new Rect (Screen.width * (0.05f + offset), Screen.height * (0.58f), Screen.width * (0.1f), Screen.height * 0.1f), counterL[i]);
				GUI.Box(new Rect (Screen.width * (0.05f + offset), Screen.height * (0.68f), Screen.width * (0.1f), Screen.height * 0.1f), countersL[i]);
				GUI.Box(new Rect (Screen.width * (0.05f + offset), Screen.height * (0.78f), Screen.width * (0.1f), Screen.height * 0.1f), icounterL[i]);
				GUI.Box(new Rect (Screen.width * (0.05f + offset), Screen.height * (0.88f), Screen.width * (0.1f), Screen.height * 0.1f), topitemL[i]);
				///R
				GUI.Box(new Rect (Screen.width * (0.55f + offset), Screen.height * (0.58f), Screen.width * (0.1f), Screen.height * 0.1f), counterR[i]);
				GUI.Box(new Rect (Screen.width * (0.55f + offset), Screen.height * (0.68f), Screen.width * (0.1f), Screen.height * 0.1f), countersR[i]);
				GUI.Box(new Rect (Screen.width * (0.55f + offset), Screen.height * (0.78f), Screen.width * (0.1f), Screen.height * 0.1f), icounterR[i]);
				GUI.Box(new Rect (Screen.width * (0.55f + offset), Screen.height * (0.88f), Screen.width * (0.1f), Screen.height * 0.1f), topitemR[i]);
				offset += 0.075f;
			}


		}
		
	}
	
	void Awake()
	{
		fileName = "DemoXmlFile";
		Champions = new List<Champion>(); // initalize champion list
		selecting = false;				//set selecting to true for first time use
		selectingLeft = false;			//selecting left first
		selectingRight = false;
		champSelectPg = 1;				//page 1 at start
		defaultC = true;		//for default champ at start
		level = 1; 				//default value for level of champion
		level2 = 1;				//default value for level of champion 2
		tipNoL = 1;
		tipNoR = 1;
	}
	
	void Start ()
	{
		loadXMLFromAssest();
		readXml();
		champLeft = Champions[PlayerPrefs.GetInt ("Player Champ")-1];
		champPortraitSmallL = (Texture)Resources.Load (Champions[PlayerPrefs.GetInt ("Player Champ")-1].portrait); //load champion selected from main screen
	}

	//function for slider feature for levels
	public void AdjustLevel(float newLevel)
	{
		level = newLevel;
		}
	//second slider
	public void AdjustLevel2(float newLevel2)
	{
		level2 = newLevel2;
	}


	// Following method load xml file from resouces folder under Assets
	private void loadXMLFromAssest()
	{
		xmlDoc = new XmlDocument();
		//if(System.IO.File.Exists(getPath()))
		//{
		//	xmlDoc.LoadXml(System.IO.File.ReadAllText(getPath()));
		//}
		//else
		//{
			textXml = (TextAsset)Resources.Load(fileName, typeof(TextAsset));
			xmlDoc.LoadXml(textXml.text);
		//}
	}
	
	void Update()
	{

	}
	// Following method reads the xml file and display its content 
	private void readXml()
	{
		foreach(XmlElement node in xmlDoc.SelectNodes("Champions/Champion"))
		{
			//Champion tempChampion = new Champion();
			tempChampion.Id = int.Parse(node.GetAttribute("id"));
			tempChampion.name = node.SelectSingleNode("name").InnerText;
			tempChampion.desc = node.SelectSingleNode("desc").InnerText;
			tempChampion.health = float.Parse(node.SelectSingleNode("health").InnerText);
			tempChampion.healthperlvl = float.Parse(node.SelectSingleNode("healthperlvl").InnerText);
			tempChampion.healthregen = float.Parse(node.SelectSingleNode("healthregen").InnerText);
			tempChampion.healthregenperlvl = float.Parse(node.SelectSingleNode("healthregenperlvl").InnerText);
			tempChampion.mana = float.Parse(node.SelectSingleNode("mana").InnerText);
			tempChampion.manaperlvl = float.Parse(node.SelectSingleNode("manaperlvl").InnerText);
			tempChampion.manaregen = float.Parse(node.SelectSingleNode("manaregen").InnerText);
			tempChampion.manaregenperlvl = float.Parse(node.SelectSingleNode("manaregenperlvl").InnerText);
			tempChampion.range = float.Parse(node.SelectSingleNode("range").InnerText);
			tempChampion.attackdamage = float.Parse(node.SelectSingleNode("attackdamage").InnerText);
			tempChampion.attackdamageperlvl = float.Parse(node.SelectSingleNode("attackdamageperlvl").InnerText);
			tempChampion.attackspeed = float.Parse(node.SelectSingleNode("attackspeed").InnerText);
			tempChampion.attackspeedperlvl = float.Parse(node.SelectSingleNode("attackspeedperlvl").InnerText);
			tempChampion.armour = float.Parse(node.SelectSingleNode("armour").InnerText);
			tempChampion.armourperlvl = float.Parse(node.SelectSingleNode("armourperlvl").InnerText);
			tempChampion.magicres = float.Parse(node.SelectSingleNode("magicres").InnerText);
			tempChampion.magicresperlvl = float.Parse(node.SelectSingleNode("magicresperlvl").InnerText);
			tempChampion.movspeed = float.Parse(node.SelectSingleNode("movspeed").InnerText);
			tempChampion.portrait = node.SelectSingleNode("portrait").InnerText;
			tempChampion.portraitL = node.SelectSingleNode("portraitL").InnerText;
			tempChampion.face = node.SelectSingleNode("face").InnerText;
			tempChampion.counter1 = node.SelectSingleNode("counter1").InnerText;
			tempChampion.counter2 = node.SelectSingleNode("counter2").InnerText;
			tempChampion.counter3 = node.SelectSingleNode("counter3").InnerText;
			tempChampion.counter4 = node.SelectSingleNode("counter4").InnerText;
			tempChampion.counter5 = node.SelectSingleNode("counter5").InnerText;
			tempChampion.counters1 = node.SelectSingleNode("counters1").InnerText;
			tempChampion.counters2 = node.SelectSingleNode("counters2").InnerText;
			tempChampion.counters3 = node.SelectSingleNode("counters3").InnerText;
			tempChampion.counters4 = node.SelectSingleNode("counters4").InnerText;
			tempChampion.counters5 = node.SelectSingleNode("counters5").InnerText;
			tempChampion.icounter1 = node.SelectSingleNode("icounter1").InnerText;
			tempChampion.icounter2 = node.SelectSingleNode("icounter2").InnerText;
			tempChampion.icounter3 = node.SelectSingleNode("icounter3").InnerText;
			tempChampion.icounter4 = node.SelectSingleNode("icounter4").InnerText;
			tempChampion.icounter5 = node.SelectSingleNode("icounter5").InnerText;
			tempChampion.topitem1 = node.SelectSingleNode("topitem1").InnerText;
			tempChampion.topitem2 = node.SelectSingleNode("topitem2").InnerText;
			tempChampion.topitem3 = node.SelectSingleNode("topitem3").InnerText;
			tempChampion.topitem4 = node.SelectSingleNode("topitem4").InnerText;
			tempChampion.topitem5 = node.SelectSingleNode("topitem5").InnerText;
			tempChampion.tip1 = node.SelectSingleNode("tip1").InnerText;
			tempChampion.tip2 = node.SelectSingleNode("tip2").InnerText;
			tempChampion.tip3 = node.SelectSingleNode("tip3").InnerText;
			tempChampion.tip4 = node.SelectSingleNode("tip4").InnerText;
			tempChampion.tip5 = node.SelectSingleNode("tip5").InnerText;
			Champions.Add(tempChampion);
		
		}
	}
	
	// Following method create new element of player
	private void createElement()
	{
		Champion tempChampion = new Champion();
		tempChampion.Id = Champions.Count+1;
		tempChampion.name = "Champion"+tempChampion.Id;
		tempChampion.health = tempChampion.Id*10;
		Champions.Add(tempChampion);
		
		XmlNode parentNode = xmlDoc.SelectSingleNode("Champions");
		XmlElement element = xmlDoc.CreateElement("Champion");
		element.SetAttribute("id",tempChampion.Id.ToString());
		element.AppendChild(createNodeByName("name",tempChampion.name));
		element.AppendChild(createNodeByName("health",tempChampion.health.ToString()));
		parentNode.AppendChild(element);
		xmlDoc.Save(getPath()+".xml");
	}
	private XmlNode createNodeByName(string name, string innerText)
	{
		XmlNode node = xmlDoc.CreateElement(name);
		node.InnerText = innerText;
		return node;
	}
	
	// Following method is used to retrive the relative path as device platform
	private string getPath(){
		#if UNITY_EDITOR
		return Application.dataPath +"/Resources/"+fileName;
		#elif UNITY_ANDROID
		return Application.persistentDataPath+fileName;
		#elif UNITY_IPHONE
		return GetiPhoneDocumentsPath()+"/"+fileName;
		#else
		return Application.dataPath +"/"+ fileName;
		#endif
	}
	private string GetiPhoneDocumentsPath()
	{
		// Strip "/Data" from path 
		string path = Application.dataPath.Substring(0, Application.dataPath.Length - 5);
		// Strip application name 
		path = path.Substring(0, path.LastIndexOf('/'));
		return path + "/Documents";
	}
}