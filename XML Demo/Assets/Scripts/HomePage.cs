﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;


public class HomePage : MonoBehaviour {

	private string path;
	private string fileInfo;
	private XmlDocument xmlDoc;
	private WWW www;
	private TextAsset textXml;
	private List<Champion> Champions;
	private string fileName;
	Champion tempChampion = new Champion();

	//textures
	private Texture title;										//lol genie title
	private Texture exit;										//exit button
	private Texture btn1;
	private Texture btn2;
	private Texture btn3;
	private Texture box;
	private Texture selectionChamp;
	public Texture face;
	private Texture arrow;										//arrow button for champ selection
	private Texture arrowR;										//arrow button for champ selection
	private Texture champSelectTex;

	//ints
	private int champSelectPg;									//page number for the genie champs #1
	private int champSelected;

	//strings
	////private string selectionChamp;

	//styles
	public GUIStyle homeBtnStyle;
	public GUIStyle selectionStyle;

	//bools 
	private bool selecting;

	struct Champion
	{
		public int Id;
		public string name;
		public string portrait;
		public string portraitL;
		public string face;
	}

	void OnGUI()
	{
		//load textures
		title = (Texture)Resources.Load ("Textures/Titles/title");
		exit = (Texture)Resources.Load ("Textures/Titles/x");
		btn1 = (Texture)Resources.Load ("Textures/Titles/genie");
		btn2 = (Texture)Resources.Load ("Textures/Titles/editor");
		btn3 = (Texture)Resources.Load ("Textures/Titles/items");
		box = (Texture)Resources.Load ("Textures/Titles/box");
		arrow = (Texture)Resources.Load ("Textures/Titles/arrowL");
		arrowR = (Texture)Resources.Load ("Textures/Titles/arrowR");
		champSelectTex = (Texture)Resources.Load ("Textures/Titles/choosechamp");

		//draw title lol genie
		GUI.DrawTexture (new Rect (Screen.width * (0.25f), Screen.height * (0.05f), Screen.width * (0.5f), Screen.height * (0.10f)), title, ScaleMode.StretchToFill, true);

		if (selecting) {
		//arrow buttons
			if (GUI.Button (new Rect (Screen.width * (0.2f), Screen.height * (0.725f), Screen.width * (0.15f), Screen.height * (0.15f)), arrow))
			{
				champSelectPg--;		
				if (champSelectPg < 1)
				{
					champSelectPg = 4;
				}
			}
			if (GUI.Button (new Rect (Screen.width * (0.65f), Screen.height * (0.725f), Screen.width * (0.15f), Screen.height * (0.15f)), arrowR))
			{
				champSelectPg++;		
				if (champSelectPg > 4)
				{
					champSelectPg = 1;
				}
			}
		
			GUI.DrawTexture (new Rect (Screen.width * (0.4f), Screen.height * (0.7f), Screen.width * (0.2f), Screen.height * (0.2f)), champSelectTex);

		float offset = 0.0f;
		float offsetTwo = 0.0f;
		float offsetThree = 0.0f;
		float offsetFour = 0.0f;
		for (int i = 0; i < Champions.Count; i++) 
		{
			face = (Texture)Resources.Load (Champions [i].face);
			switch (champSelectPg)
			{
			case 1:
				if (1 < 9)
				{
					if (GUI.Button (new Rect (Screen.width * (0.05f + offset), Screen.height * (0.25f), Screen.width * (0.1f), Screen.height * (0.1f)), face))
					{
						selectionChamp = (Texture)Resources.Load (Champions [i].portrait);
						selecting = false;
						champSelected = Champions[i].Id;
					}
				
				offset += 0.1f;
				}
				if (9 <= i && i < 18)
				{
					if (GUI.Button (new Rect (Screen.width * (0.05f + offsetTwo), Screen.height * (0.35f), Screen.width * (0.1f), Screen.height * (0.1f)), face))
					{
						selectionChamp = (Texture)Resources.Load (Champions [i].portrait);
						selecting = false;
							champSelected = Champions[i].Id;
					}
					offsetTwo += 0.1f;
				}
				if (18 <= i && i < 27)
				{
					if (GUI.Button (new Rect (Screen.width * (0.05f + offsetThree), Screen.height * (0.45f), Screen.width * (0.1f), Screen.height * (0.1f)), face))
					{
						selectionChamp = (Texture)Resources.Load (Champions [i].portrait);
						selecting = false;
							champSelected = Champions[i].Id;
					}
					offsetThree += 0.1f;
				}
				if (27 <= i && i < 36)
				{
					if (GUI.Button (new Rect (Screen.width * (0.05f + offsetFour), Screen.height * (0.55f), Screen.width * (0.1f), Screen.height * (0.1f)), face))
					{
						selectionChamp = (Texture)Resources.Load (Champions [i].portrait);
						selecting = false;
							champSelected = Champions[i].Id;
					}
					offsetFour += 0.1f;
				}
				break;

				case 2:
					if (36 <= i && i < 45)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offset), Screen.height * (0.25f), Screen.width * (0.1f), Screen.height * (0.1f)), face))
						{
							selectionChamp = (Texture)Resources.Load (Champions [i].portrait);
							selecting = false;
							champSelected = Champions[i].Id;
						}
						
						offset += 0.1f;
					}
					if (/*54*/45 <= i && i < 54/*63*/)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offsetTwo), Screen.height * (0.35f), Screen.width * (0.1f), Screen.height * (0.1f)), face))
						{
							selectionChamp = (Texture)Resources.Load (Champions [i].portrait);
							selecting = false;
							champSelected = Champions[i].Id;
						}
						offsetTwo += 0.1f;
					}
					if (/*72*/54 <= i && i < /*81*/63)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offsetThree), Screen.height * (0.45f), Screen.width * (0.1f), Screen.height * (0.1f)), face))
						{
							selectionChamp = (Texture)Resources.Load (Champions [i].portrait);
							selecting = false;
							champSelected = Champions[i].Id;
						}
						offsetThree += 0.1f;
					}
					if (/*90*/63 <= i && i < /*99*/72)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offsetFour), Screen.height * (0.55f), Screen.width * (0.1f), Screen.height * (0.1f)), face))
						{
							selectionChamp = (Texture)Resources.Load (Champions [i].portrait);
							selecting = false;
							champSelected = Champions[i].Id;
						}
						offsetFour += 0.1f;
					}
					break;

				case 3:
					if (/*99*/72 <= i && i < 81/*108*/)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offset), Screen.height * (0.25f), Screen.width * (0.1f), Screen.height * (0.1f)), face))
						{
							selectionChamp = (Texture)Resources.Load (Champions [i].portrait);
							selecting = false;
							champSelected = Champions[i].Id;
						}
						
						offset += 0.1f;
					}
					if (/*117*/81 <= i && i < /*126*/90)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offsetTwo), Screen.height * (0.35f), Screen.width * (0.1f), Screen.height * (0.1f)), face))
						{
							selectionChamp = (Texture)Resources.Load (Champions [i].portrait);
							selecting = false;
							champSelected = Champions[i].Id;
						}
						offsetTwo += 0.1f;
					}
					if (/*135*/90 <= i && i < /*144*/99)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offsetThree), Screen.height * (0.45f), Screen.width * (0.1f), Screen.height * (0.1f)), face))
						{
							selectionChamp = (Texture)Resources.Load (Champions [i].portrait);
							selecting = false;
							champSelected = Champions[i].Id;
						}
						offsetThree += 0.1f;
					}
					if (/*153*/99 <= i && i < /*162*/108)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offsetFour), Screen.height * (0.55f), Screen.width * (0.1f), Screen.height * (0.1f)), face))
						{
							selectionChamp = (Texture)Resources.Load (Champions [i].portrait);
							selecting = false;
							champSelected = Champions[i].Id;
						}
						offsetFour += 0.1f;
					}
					break;
					
				case 4:
					if (/*99*/108 <= i && i < 117/*108*/)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offset), Screen.height * (0.25f), Screen.width * (0.1f), Screen.height * (0.1f)), face))
						{
							selectionChamp = (Texture)Resources.Load (Champions [i].portrait);
							selecting = false;
							champSelected = Champions[i].Id;
						}
						
						offset += 0.1f;
					}
					if (/*117*/117 <= i && i < /*126*/126)
					{
						if (GUI.Button (new Rect (Screen.width * (0.05f + offsetTwo), Screen.height * (0.35f), Screen.width * (0.1f), Screen.height * (0.1f)), face))
						{
							selectionChamp = (Texture)Resources.Load (Champions [i].portrait);
							selecting = false;
							champSelected = Champions[i].Id;
						}
						offsetTwo += 0.1f;
					}
					break;


			}
	
		}


		} else {

			//champion selection
			GUI.DrawTexture (new Rect (Screen.width * (0.425f), Screen.height * (0.25f), Screen.width * (0.15f), Screen.height * (0.35f)), selectionChamp, ScaleMode.StretchToFill, true);
			if (GUI.Button (new Rect (Screen.width * (0.425f), Screen.height * (0.25f), Screen.width * (0.15f), Screen.height * (0.35f)), "", selectionStyle)) 
			{
				selecting = true;
			}

			///GUI.Box (new Rect (Screen.width * (0.4f), Screen.height * (0.20f), Screen.width * (0.2f), Screen.height * (0.2f)), box);
			//GUI.Box (new Rect (Screen.width * (0.4f), Screen.height * (0.45f), Screen.width * (0.2f), Screen.height * (0.2f)), box);
			GUI.Box (new Rect (Screen.width * (0.4f), Screen.height * (0.7f), Screen.width * (0.2f), Screen.height * (0.2f)), box);
			if (GUI.Button (new Rect (Screen.width * (0.4f), Screen.height * (0.7f), Screen.width * (0.2f), Screen.height * (0.2f)), btn1))
			{
					Application.LoadLevel ("DemoScene");
			}
						///if (GUI.Button (new Rect (Screen.width * (0.4f), Screen.height * (0.45f), Screen.width * (0.2f), Screen.height * (0.2f)), btn2)) 
						//{
						///	Application.LoadLevel("DemoScene");
						////}
						//if (GUI.Button (new Rect (Screen.width * (0.4f), Screen.height * (0.7f), Screen.width * (0.2f), Screen.height * (0.2f)), btn3)) 
						///{
						//	Application.LoadLevel("DemoScene");
						//}
		
				}
		//exit button
		if (GUI.Button (new Rect (Screen.width * (0.85f), Screen.height * (0.05f), Screen.width * (0.1f), Screen.height * (0.1f)), exit)) 
		{
			Application.Quit();	
		} 

	}

	void Awake ()
	{
		fileName = "DemoXmlFile";
		Champions = new List<Champion>(); // initalize champion list
	}

	// Use this for initialization
	void Start () {
		loadXMLFromAssest();
		readXml();
		selecting = true;
		champSelectPg = 1;				//page 1 at start
	}
	
	// Update is called once per frame
	void Update () {
		PlayerPrefs.SetInt ("Player Champ", champSelected);
	}

	// Following method load xml file from resouces folder under Assets
	private void loadXMLFromAssest()
	{
		xmlDoc = new XmlDocument();
		//if(System.IO.File.Exists(getPath()))
		//{
		//	xmlDoc.LoadXml(System.IO.File.ReadAllText(getPath()));
		//}
		//else
		//{
		textXml = (TextAsset)Resources.Load(fileName, typeof(TextAsset));
		xmlDoc.LoadXml(textXml.text);
		//}
	}

	// Following method reads the xml file and display its content 
	private void readXml()
	{
		foreach(XmlElement node in xmlDoc.SelectNodes("Champions/Champion"))
		{
			tempChampion.Id = int.Parse(node.GetAttribute("id"));
			tempChampion.name = node.SelectSingleNode("name").InnerText;
			tempChampion.portrait = node.SelectSingleNode("portrait").InnerText;
			tempChampion.portraitL = node.SelectSingleNode("portraitL").InnerText;
			tempChampion.face = node.SelectSingleNode("face").InnerText;
			Champions.Add(tempChampion);

		}
	}

	// Following method is used to retrive the relative path as device platform
	private string getPath(){
		#if UNITY_EDITOR
		return Application.dataPath +"/Resources/"+fileName;

		#elif UNITY_ANDROID
		return Application.persistentDataPath+fileName;
		#elif UNITY_IPHONE
		return GetiPhoneDocumentsPath()+"/"+fileName;
		#else
		return Application.dataPath +"/"+ fileName;
		#endif
	}
	private string GetiPhoneDocumentsPath()
	{
		// Strip "/Data" from path 
		string path = Application.dataPath.Substring(0, Application.dataPath.Length - 5);
		// Strip application name 
		path = path.Substring(0, path.LastIndexOf('/'));
		return path + "/Documents";
	}
}
